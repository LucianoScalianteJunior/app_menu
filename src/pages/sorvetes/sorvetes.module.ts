import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SorvetesPage } from './sorvetes';

@NgModule({
  declarations: [
    SorvetesPage,
  ],
  imports: [
    IonicPageModule.forChild(SorvetesPage),
  ],
})
export class SorvetesPageModule {}
