import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LanchesPage } from '../lanches/lanches';
import { SucosPage } from '../sucos/sucos';
import { BebidasPage } from '../bebidas/bebidas';
import { SorvetesPage } from '../sorvetes/sorvetes';
import { SobbremesasPage } from '../sobbremesas/sobbremesas';
import { RefeicaoPage } from '../refeicao/refeicao';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pages: Array<{ title: string, img: string, component: any }>;

  constructor(public navCtrl: NavController) {

    this.pages = [
      { title: 'Lanches', img: '../../assets/imgs/lanche-ico.jpeg', component: LanchesPage },
      { title: 'Sucos', img: '../../assets/imgs/sucos-ico.jpeg', component: SucosPage },
      { title: 'Bebidas', img: '../../assets/imgs/bebidas-ico.jpeg', component: BebidasPage },
      { title: 'Sorvetes', img: '../../assets/imgs/sorvetes-ico.jpeg', component: SorvetesPage },
      { title: 'Sobremesa', img: '../../assets/imgs/Chocolate-cake-piece-raspberries_m.jpg', component: SobbremesasPage },
      { title: 'Refeições', img: '../../assets/imgs/refeicoes-empresas-sp-03.jpg', component: RefeicaoPage }
    ];

  }

  openPage(page: any): void {
    this.navCtrl.push(page.component);
  }

  openBebidas() {
    this.navCtrl.push(BebidasPage);
  }

}
