import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SobbremesasPage } from './sobbremesas';

@NgModule({
  declarations: [
    SobbremesasPage,
  ],
  imports: [
    IonicPageModule.forChild(SobbremesasPage),
  ],
})
export class SobbremesasPageModule {}
